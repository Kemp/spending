package com.hb.bankstatement.spending.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SpendingResponseModel implements Serializable {

    private static final long serialVersionUID = -5742279835379471488L;

    @JsonIgnore
    private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

    @JsonIgnore
    private BigDecimal total = BigDecimal.ZERO;

    private String period;
    private String totalSpend;
    private List<AccountTotalSpentModel> accountTotals = new ArrayList<>();

    public SpendingResponseModel(String spendPeriodYear, String spendPeriodMonth) {
        period = spendPeriodMonth + "-" + spendPeriodYear;
        totalSpend = currencyFormat.format(total);
    }

    public void addAccountSpendingTotals(AccountTotalSpentModel accountTotal) {
        accountTotals.add(accountTotal);
        total = total.add(accountTotal.getAccountSpending());
        totalSpend = currencyFormat.format(total);
    }

    public void setAccountTotals(List<AccountTotalSpentModel> accountTotals) {
        accountTotals.forEach(this::addAccountSpendingTotals);
    }

}
