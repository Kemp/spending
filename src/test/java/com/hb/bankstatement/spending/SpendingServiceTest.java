package com.hb.bankstatement.spending;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hb.bankstatement.spending.model.SpendingResponseModel;
import com.hb.bankstatement.spending.model.StarlingAccountModel;
import com.hb.bankstatement.spending.model.StarlingAccountsModel;
import com.hb.bankstatement.spending.model.StarlingCounterSpendingModel;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;


class SpendingServiceTest {

    public static MockWebServer mockWebServer;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private SpendingService classUnderTest;

    @BeforeAll
    static void setup() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @BeforeEach
    void init() {
        String url = "http://localhost:" + mockWebServer.getPort();
        String accountsEndpoint = "/api/v2/accounts";
        String spendingEndpoint = "/spending-insights/counter-party";
        String defaultYear = "2020";
        String defaultMonth = "May";

        classUnderTest = new SpendingService(url, accountsEndpoint, spendingEndpoint, defaultYear, defaultMonth);
    }

    @Test
    void testGetTotalSpendingWhenInvalidArguments() {
        StepVerifier.create(classUnderTest.getTotalSpending(null)).verifyErrorMessage("Invalid request - access token is required");
        StepVerifier.create(classUnderTest.getTotalSpending("")).verifyErrorMessage("Invalid request - access token is required");
        StepVerifier.create(classUnderTest.getTotalSpending("abcdefg", "", "june")).verifyErrorMessage("Invalid request - valid year in the range 2014 - 2021 is required");
        StepVerifier.create(classUnderTest.getTotalSpending("abcdefg", "2016", "")).verifyErrorMessage("Invalid request - valid month is required");
    }

    @Test
    void testGetTotalAccountsSpendingForPeriodWhenOneAccountFound() throws JsonProcessingException {
        StarlingAccountModel mockAccount = StarlingAccountModel.builder()
                .accountType("mockaccounttype")
                .accountUid("mockaccountuid")
                .createdAt("rightnow")
                .currency("GBP")
                .defaultCategory("mockcategory")
                .name("mockname")
                .build();

        StarlingAccountsModel mockAccounts = new StarlingAccountsModel();
        mockAccounts.setAccounts(Collections.singletonList(mockAccount));

        StarlingCounterSpendingModel mockSpending = StarlingCounterSpendingModel.builder()
                .period("2021-07")
                .totalSpent(new BigDecimal("123.45"))
                .totalReceived(BigDecimal.ZERO)
                .netSpend(new BigDecimal("123.45"))
                .totalSpendNetOut(BigDecimal.ONE)
                .totalReceivedNetIn(BigDecimal.ZERO)
                .currency("GBP")
                .direction("IN")
                .breakdown(Collections.emptyList())
                .build();

        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(mockAccounts))
                .addHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE));
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(mockSpending))
                .addHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE));

        Mono<SpendingResponseModel> spendingResponse = classUnderTest.getTotalAccountsSpendingForPeriod("mocktoken", "anyyear", "anymonth");

        StepVerifier.create(spendingResponse)
                .expectNextMatches(spending -> spending.getTotalSpend().equals("£123.45")).verifyComplete();

    }

    @Test
    void testValidYear() {
        assertTrue(classUnderTest.validYear("2014"));
        assertTrue(classUnderTest.validYear("2021"));
        assertFalse(classUnderTest.validYear("2013"));
        assertFalse(classUnderTest.validYear("2022"));
        assertFalse(classUnderTest.validYear("abc123"));
    }

    @Test
    void testValidMonth() {
        assertTrue(classUnderTest.validMonth("July", "2014"));
        assertTrue(classUnderTest.validMonth("JUNE", "2020"));
        assertTrue(classUnderTest.validMonth("january", "2021"));
        assertFalse(classUnderTest.validMonth("jan", "2021"));
        assertFalse(classUnderTest.validMonth("01", "2021"));
        assertFalse(classUnderTest.validMonth("month", "2019"));

        LocalDate futureDate = LocalDate.now().plusMonths(1L);
        assertFalse(classUnderTest.validMonth(futureDate.getMonth().name(), String.valueOf(futureDate.getYear())));
    }



}