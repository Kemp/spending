package com.hb.bankstatement.spending.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StarlingCounterSpendingModel implements Serializable {

    private static final long serialVersionUID = 5380607306314806409L;

    private String period;
    private BigDecimal totalSpent;
    private BigDecimal totalReceived;
    private BigDecimal netSpend;
    private BigDecimal totalSpendNetOut;
    private BigDecimal totalReceivedNetIn;
    private String currency;
    private String direction;
    private List<StarlingCounterSpendingBreakdownModel> breakdown;

    @JsonIgnore
    private String accountUid;

}
