package com.hb.bankstatement.spending;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.hb.bankstatement.spending.model.AccountTotalSpentModel;
import com.hb.bankstatement.spending.model.SpendingResponseModel;
import com.hb.bankstatement.spending.model.StarlingAccountsModel;
import com.hb.bankstatement.spending.model.StarlingCounterSpendingModel;

import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Log4j2
@Service
public class SpendingService {

    private final WebClient webClient;
    private final String accountsEndpoint;
    private final String spendingEndpoint;
    private final String defaultYear;
    private final String defaultMonth;

    private final DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern("dd MMMM yyyy").toFormatter();

    @Autowired
    public SpendingService(
            @Value("${starling.base.url}") String baseUrl,
            @Value("${starling.endpoint.accounts}") String accountsEndpoint,
            @Value("${starling.endpoint.spending}") String spendingEndpoint,
            @Value("${spendingapp.spend.period.default.year}") String defaultYear,
            @Value("${spendingapp.spend.period.default.month}") String defaultMonth) {

        webClient = WebClient.create(baseUrl);
        this.accountsEndpoint = accountsEndpoint;
        this.spendingEndpoint = spendingEndpoint;
        this.defaultYear = defaultYear;
        this.defaultMonth = defaultMonth;
    }

    public Mono<SpendingResponseModel> getTotalSpending(String accessToken) {
        return getTotalSpending(accessToken, defaultYear, defaultMonth);
    }

    public Mono<SpendingResponseModel> getTotalSpending(String accessToken, String year, String month) {
        if(isNullOrEmpty(accessToken)) {
            log.warn("getTotalSpending request with missing accessToken");
            return Mono.error(new RuntimeException("Invalid request - access token is required"));
        }
        if(isNullOrEmpty(year) || !validYear(year)) {
            log.warn("getTotalSpending request with invalid year[{}]", year);
            return Mono.error(new RuntimeException("Invalid request - valid year in the range 2014 - 2021 is required"));
        }
        if(isNullOrEmpty(month) || !validMonth(month, year)) {
            log.warn("getTotalSpending request with invalid month[{}]", month);
            return Mono.error(new RuntimeException("Invalid request - valid month is required"));
        }
        return getTotalAccountsSpendingForPeriod(accessToken, year, month.toUpperCase());
    }

    protected boolean validYear(String year) {
        boolean valid = false;
        try {
            int yearNumber = Integer.parseInt(year);
            // Starling bank was founded in 2014 so would not expect any transactions prior to that
            valid = yearNumber >= 2014 && yearNumber <= LocalDate.now().getYear();
        } catch (NumberFormatException nfe) {
            log.error("Failed to parse year[{}]", year);
        }
        return valid;
    }

    protected boolean validMonth(String month, String year) {
        boolean valid = false;
        String dateToCheck = "01 " + month + " " + Integer.parseInt(year);
        try {
            LocalDate localDate = LocalDate.from(dateTimeFormatter.parse(dateToCheck));
            LocalDate now = LocalDate.now();
            valid = localDate.isBefore(now) || localDate.isEqual(now);
        } catch (DateTimeParseException ex) {
            log.error("Failed to parse month[{}] - parse exception: {}", month, ex.getMessage());
        }
        return valid;
    }

    protected Mono<SpendingResponseModel> getTotalAccountsSpendingForPeriod(String accessToken, String year, String month) {
        if(isNullOrEmpty(accessToken) || isNullOrEmpty(year) || isNullOrEmpty(month)) {
            return Mono.error(new RuntimeException("Invalid request - missing required parameters"));
        }
        final Mono<SpendingResponseModel> spendingResponse = Mono.just(new SpendingResponseModel(year, month));
        Mono<List<AccountTotalSpentModel>> accountSpentModels = Flux.from(getUserAccountUIDs(accessToken).flatMapIterable(StarlingAccountsModel::getAccounts))
                .parallel()
                .runOn(Schedulers.parallel())
                .flatMap(account -> getCounterSpendingModel(account.getAccountUid(), accessToken, year, month))
                .filter(spendingModel -> "GBP".equals(spendingModel.getCurrency()))
                .map(spendingModel -> new AccountTotalSpentModel(spendingModel.getAccountUid(), spendingModel.getTotalSpent(), "GBP"))
                .sequential()
                .collectList();

        return spendingResponse.zipWith(accountSpentModels).map(tuple -> {
            SpendingResponseModel sr = tuple.getT1();
            sr.setAccountTotals(tuple.getT2());
            return sr;
        });
    }

    private Mono<StarlingAccountsModel> getUserAccountUIDs(String accessToken) {
        return webClient
                .get()
                .uri(accountsEndpoint)
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(StarlingAccountsModel.class);
    }

    private Mono<StarlingCounterSpendingModel> getCounterSpendingModel(String accountUID, String authToken, String year, String month) {
        return webClient
                .get()
                .uri(accountsEndpoint + "/" + accountUID + spendingEndpoint + "?year=" + year + "&month=" + month)
                .header("Authorization", "Bearer " + authToken)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(StarlingCounterSpendingModel.class)
                .doOnNext(model -> model.setAccountUid(accountUID));
    }

    protected boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

}
