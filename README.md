# Holland & Barrett Technical Test

---
The task is to implement an API by spring boot:  
* the total spending of all my accounts last month; 
* now it's June, so to calculate the total spending in May 
* A readme is needed how to run the app. 
* This service should be production-ready 
  
Reference: 
* https://developer.starlingbank.com/docs
* Endpoint to use: https://api-sandbox.starlingbank.com/api/v2

---

### How to run:

Open a terminal at the root directory of this project.

1. ./gradlew build -- to build the solution
2. ./gradlew test -- to test the solution
3. ./gradlew bootRun -- to run the solution
4. ./gradlew clean -- to clean the solution

Alternatively open the project with IntelliJ, Eclipse or any other IDE of your choice and access the gradle tasks through it. 

_Note: Gradle version should be Gradle 7.1.1+_

The endpoint to access will be either:

http://localhost:8080/api/v1/spending (which will use the default month and year as specified in src/main/resources/application.properties)

or with the year and month as additional path parameters

http://localhost:8080/api/v1/spending/2021/June 

The account access token must be specified as a header named 'accessToken'

e.g. 

curl --location --request GET 'http://localhost:8080/api/v1/spending/2021/June' \
--header 'accessToken: really long string provided by Starling'


---

### Assumptions:

- Allowing user to specify month and year of statements, with month only being entered in English.
- Only need to total spending in one currency, which in this case is GBP. 
- User will have their own access token


_Note: I would like to add more unit tests, but I just haven't got the time at the moment_