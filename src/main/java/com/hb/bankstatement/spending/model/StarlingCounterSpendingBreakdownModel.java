package com.hb.bankstatement.spending.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StarlingCounterSpendingBreakdownModel implements Serializable {

    private static final long serialVersionUID = 6041225447358047087L;

    private String counterPartyUid;
    private String counterPartyType;
    private String counterPartyName;
    private BigDecimal totalSpent;
    private BigDecimal totalReceived;
    private BigDecimal netSpend;
    private String netDirection;
    private String currency;
    private BigDecimal percentage;
    private int transactionCount;

}
