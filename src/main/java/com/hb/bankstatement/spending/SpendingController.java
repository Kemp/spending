package com.hb.bankstatement.spending;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hb.bankstatement.spending.model.SpendingResponseModel;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/spending")
public class SpendingController {

    private final SpendingService spendingService;

    @Autowired
    public SpendingController(final SpendingService spendingService) {
        this.spendingService = spendingService;
    }

    @GetMapping
    public Mono<SpendingResponseModel> getTotalSpending(@RequestHeader("accessToken") String accessToken) {
        return spendingService.getTotalSpending(accessToken);
    }

    @GetMapping("/{year}/{month}")
    public Mono<SpendingResponseModel> getTotalSpending(@PathVariable("year") String year, @PathVariable("month") String month, @RequestHeader("accessToken") String accessToken) {
        return spendingService.getTotalSpending(accessToken, year, month);
    }

}
