package com.hb.bankstatement.spending.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountTotalSpentModel implements Serializable {

    private static final long serialVersionUID = -3924801714816936649L;

    private String accountId;
    private BigDecimal accountSpending;
    private String currency;

}
