package com.hb.bankstatement.spending.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StarlingAccountsModel implements Serializable {

    private static final long serialVersionUID = -785473958001563569L;

    private List<StarlingAccountModel> accounts;

}
